cordova.define('cordova/plugin_list', function(require, exports, module) {
  module.exports = [
    {
      "id": "cordova-sqlite-evcore-extbuild-free.SQLitePlugin",
      "file": "plugins/cordova-sqlite-evcore-extbuild-free/www/SQLitePlugin.js",
      "pluginId": "cordova-sqlite-evcore-extbuild-free",
      "clobbers": [
        "SQLitePlugin"
      ]
    }
  ];
  module.exports.metadata = {
    "cordova-plugin-whitelist": "1.3.4",
    "cordova-sqlite-evcore-extbuild-free": "0.11.0"
  };
});