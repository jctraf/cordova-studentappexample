let db = null;

let onDeviceReady = function() {
    // Now safe to use device APIs
    console.log("device is ready");
    alert("device is ready!");

    connectToDatabase();
}


let connectToDatabase = function() {
    if (window.cordova.platformId === "browser") {
        console.log("browser detected...");
        // For browsers, use this syntax:
        //  (nameOfDb, version number, description, db size)
        // By default, set version to 1.0, and size to 2MB
        db = window.openDatabase("college", "1.0", "Database for the college", 2*1024*1024);
      }
      else {
        alert("mobile device detected");
        console.log("mobile device detected!");
        var databaseDetails = {"name":"college.db", "location":"default"}
        db = window.sqlitePlugin.openDatabase(databaseDetails);
        console.log("done opening db");
        alert("done opening db");
      }
    
      if (!db) {
        alert("databse not opened!");
        console.log("databse not opened!");
        return false;
      }
      else {
        console.log("database created!");
        alert("database created!");        
      }

    
      // create a table
      db.transaction(
		function(tx){
			// Execute the SQL via a usually anonymous function 
			// tx.executeSql( SQL string, arrary of arguments, success callback function, failure callback function)
			// To keep it simple I've added to functions below called onSuccessExecuteSql() and onFailureExecuteSql()
			// to be used in the callbacks
			tx.executeSql(
				"CREATE TABLE IF NOT EXISTS students (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, gpa DOUBLE(2,1) )",
				[],
				onSuccessExecuteSql,
				onError
			)
		},
		onError,
		onReadyTransaction
	)
      
}


// Other common database functions
let onReadyTransaction = function() {
    console.log("Transaction completed");
    alert("Transaction completed!");
}
let onSuccessExecuteSql = function(tx, results) {
    console.log("Execute SQL completed");
    alert("Execute SQL completed");
}

let onError = function(err) {
    console.log(err);
    alert(err);
}


let addStudentPressed = function() {
    alert("Add student button pressed!");
    
    let studentName = document.getElementById("text-name").value;
    let studentGPA = parseFloat(document.getElementById("text-gpa").value);

    // add a student to the database
    db.transaction(
		function(tx){
			tx.executeSql("INSERT INTO students(name,gpa) VALUES(?, ?)",
			                [studentName, studentGPA],
			                onSuccessExecuteSql,
                            onError
                        )
		},
		onError,
		onReadyTransaction
	)

    alert("Done adding student!");
}

let showStudentsPressed = function() {
    alert("Show students button pressed!");
    
    // add a student to the database
    db.transaction(
		function(tx){
            tx.executeSql("SELECT * FROM students",
                            [],
                            showStudentsResults,
			                onSuccessExecuteSql,
                            onError
                        )
		},
		onError,
		onReadyTransaction
	)

    alert("Done adding student!");
}

let showStudentsResults  = function(tx, results) {
    alert("Got student results!");

    // SELECT query returned no results
    if(results.rows.length == 0) {
        alert("No records found");
        return false;
    }
    
    // SELECT query has results, so process them
    let output = "";
    for(var i=0; i<results.rows.length; i++) {
        let row  = results.rows.item(i);
        let id = row.id;
        let name = row.name;
        let gpa = row.gpa;

        console.log(`Row id: ${id}, Student Name: ${name}, GPA: ${gpa}`);
        output += `Row id: ${id}, Student Name: ${name}, GPA: ${gpa}\n`;
    }
    alert(output);
    
}


document.addEventListener("deviceready", onDeviceReady, false);
document.getElementById("btn-add-student").addEventListener("click", addStudentPressed);
document.getElementById("btn-load-students").addEventListener("click", showStudentsPressed);